# ESP32 DevKit Demo1 #

## Resources on the dev board ##

[ESP32_Wrover Getting Started](https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/get-started-wrover-kit.html#get-started-esp-wrover-kit-v4-1-lcd-connector)

[PIO ESP32 Page](https://docs.platformio.org/en/latest/boards/espressif32/esp-wrover-kit.html)

## Infos on PlatformIO and VSCode ##

[PIO QuickStart for VSCode](https://docs.platformio.org/en/latest/ide/vscode.html#quick-start)

## Téléchargements ##

[VSCode](https://code.visualstudio.com/)

[PlatformIO](https://platformio.org/)

[Python Downloads](https://www.python.org/downloads/release/python-374#Files)